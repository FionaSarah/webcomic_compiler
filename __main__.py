#!/usr/bin/env python
"""
-------------------------------
Webcomic Compiler
2016 Summers Development
-------------------------------

A tool for leeching whole webcomic archives
and compiling them into CBZ files for reading
in a comic reader.
"""

import os
import sys
import argparse
import time
import math
import pkgutil
import shutil
import glob
import string
import textwrap
import urllib.request
from urllib.error import URLError
from requests.exceptions import ConnectionError, HTTPError, Timeout
import mechanicalsoup
from PIL import Image, ImageFont, ImageDraw
from io import BytesIO, StringIO

class WebcomicCompilerApp:

    saved_archive_dir = "archives"
    compiled_comics_dir = "compiled"
    volume_info_dir = "volumes"
    resume_info_filename = "resume"
    page_number_padding= 5
    sleep_time_between_pages = 2
    default_retries = 5
    image_type_to_save = "png"
    default_archive_type = "zip"
    default_pages_per_collection = 100
    alt_text_font = "DejaVuSansMono.ttf"
    alt_text_size = 28
    alt_text_padding = 10

    browser = None

    def __init__(self):
        """
        Entry point
        """
        main_parser = argparse.ArgumentParser(description='Webcomic compiler')
        subparsers = main_parser.add_subparsers(help='Available commands')

        parser_1 = subparsers.add_parser('list', help='List webcomic archives supported.')
        parser_1.set_defaults(func=self.list_task)

        parser_1 = subparsers.add_parser('download', help='Downloads a webcomic archive. Pass a comic name (ie: download scarygoround).')
        parser_1.set_defaults(func=self.download_task)
        parser_1.add_argument("webcomic")
        parser_1.add_argument("--resume", action="store_true", help='Attempt to resume an archive download already started.')
        parser_1.add_argument("--start", type=str, help='Archive page to start fram. (Overriden by -resume)')
        parser_1.add_argument("--limit", type=int, help='Limit the number of camics to get.')
        parser_1.add_argument("--sleep", type=int, default=self.sleep_time_between_pages, help='How many seconds to wait between retrieving pages.')
        parser_1.add_argument("--retry", type=int, default=self.default_retries, help='How many times to retry failed fetches.')
        parser_1.add_argument("--image_type", type=str, default=self.image_type_to_save, help='Image type to save comics as (as extension, ie - png, gif, tiff) Default: png')
        parser_1.add_argument("--no_alt_text", action="store_true", help='Wont add any found alt-text on the bottom of the pages.')

        parser_2 = subparsers.add_parser('compile', help='Compiles a downloaded archive into comic files as volumes or collections. Pass a comic name (ie: compile scarygoround).')
        parser_2.set_defaults(func=self.compile_task)
        parser_2.add_argument("webcomic")
        parser_2.add_argument("--collections", action="store_true", help='Compiles comics into collections of pages, instead of volumes or chapters. If '\
                              'a comic does not have volume information this will be done by default.')
        parser_2.add_argument("--pages_per_collection", type=int, default=self.default_pages_per_collection, help='Number of comic pages that will be put into each collection.')

        WebcomicCompilerApp.browser = mechanicalsoup.Browser()

        if len(sys.argv[1:])==0:
            main_parser.print_help()
            main_parser.exit()
        args = main_parser.parse_args()
        args.func(args)

    def list_task(self, parsed_args):
        """
        Lists webcomics that the compiler supports
        """
        print("Available webcomics for downloading archives from - ")
        print("----------")
        for _, name, _ in pkgutil.iter_modules(["webcomics"]):
            webcomic = WebcomicCompilerApp.get_webcomic_object(name)
            print("%s - %s by %s (%s)" % (name, webcomic.name, webcomic.author, webcomic.url))

    def download_task(self, parsed_args):
        """
        Download a comic's archive to the filesystem
        """
        self.retry = parsed_args.retry
        self.image_type = parsed_args.image_type
        self.no_alt_text = parsed_args.no_alt_text

        # Get webcomic object
        try:
            webcomic = WebcomicCompilerApp.get_webcomic_object(parsed_args.webcomic)
        except ImportError:
            print("Webcomic '%s' not supported." % (parsed_args.webcomic))
            return
        # Spit out name
        print("Downloading %s by %s" % (webcomic.name, webcomic.author))
        print("(%s)" % (webcomic.url))
        if webcomic.donate_url:
            print("Donate: %s" % (webcomic.donate_url))

        webcomic.before_download()

        current_page_url = None
        num = 1
        local_webcomic_archive_path = os.path.join(self.saved_archive_dir, parsed_args.webcomic)

        # If resuming we need to grab where to start at
        resume_file_path = os.path.join(local_webcomic_archive_path, self.resume_info_filename)
        if parsed_args.resume and os.path.exists(resume_file_path):
            with open(resume_file_path, "r") as resume_file:
                current_page_url, num = resume_file.readlines()
                current_page_url = current_page_url.strip()
                num = int(num.strip())
                print("Resuming from %s with URL %s..." % (num, current_page_url))
        else:
            # If not resuming then we need to set up filesystem
            if not os.path.exists(self.saved_archive_dir):
                os.mkdir(self.saved_archive_dir)

            if os.path.exists(local_webcomic_archive_path):
                shutil.rmtree(local_webcomic_archive_path)
            os.mkdir(local_webcomic_archive_path)
            os.mkdir(os.path.join(local_webcomic_archive_path, self.volume_info_dir))

            current_page_url = webcomic.archive_start_url() if parsed_args.start is None else parsed_args.start

        # Iterate until we no longer have any more pages
        while current_page_url:
            # Get image from page
            print(" ")
            page_content = WebcomicCompilerApp.read_url_content(current_page_url, self.retry)
            if page_content is None:
                print("Error: Couldn't fetch URL %s" % (current_page_url))
                break
            page_content = page_content.text
            image_url = webcomic.get_image_url_from_page(page_content)
            if image_url is None:
                print("Error: Couldn't find image URL for page %s" % (current_page_url))
                break
            file_extension = image_url.split(".")[-1]

            # Fetch and save image
            image_data = WebcomicCompilerApp.read_url_content(image_url, self.retry, stream=True)
            if image_data is None:
                print("Error: Couldn't fetch URL %s" % (image_url))
                break

            # Feed to PIL for additional processing
            image = Image.open(BytesIO(image_data.raw.data))

            # Add alt-text if applicable
            if not self.no_alt_text:
                alt_text = webcomic.get_alt_text_from_page(page_content)
                if alt_text is not None:
                    image = self.add_alt_text_to_page(image, alt_text)

            if file_extension != self.image_type:
                file_extension = self.image_type

            local_filename = str(num).rjust(self.page_number_padding, '0') + "." + file_extension
            local_file_path = os.path.join(self.saved_archive_dir, parsed_args.webcomic, local_filename)

            print("Saving comic as %s" % (local_file_path))
            image.save(local_file_path)

            # Get and store volume info
            volume = webcomic.get_volume_for_page(current_page_url, page_content)
            if volume is not None:
                volume = WebcomicCompilerApp.escape_filename(volume)
                with open(os.path.join(local_webcomic_archive_path, self.volume_info_dir, volume), "a") as volume_file:
                    volume_file.write(local_filename + "\n")

            # Get url for next page
            num+=1
            current_page_url = webcomic.get_next_url_from_page(page_content)

            # Save resume data
            with open(os.path.join(local_webcomic_archive_path, self.resume_info_filename), "w") as resume_file:
                resume_file.write("%s\n%s" % (current_page_url, num))

            # Finish if limited
            if not parsed_args.limit is None and num > parsed_args.limit:
                current_page_url = None

            time.sleep(parsed_args.sleep)

        webcomic.after_download()

        print("Archive finished downloading")

    def compile_task(self, parsed_args):
        """
        Task that compiles comic pages into collections or volumes.
        """
        archive_type = self.default_archive_type
        collections = parsed_args.collections
        pages_per_collection = parsed_args.pages_per_collection

        try:
            webcomic = WebcomicCompilerApp.get_webcomic_object(parsed_args.webcomic)
        except ImportError:
            print("Webcomic '%s' not supported." % (parsed_args.webcomic))
            return

        # Spit out name
        print("Compiling %s by %s" % (webcomic.name, webcomic.author))
        print("(%s)" % (webcomic.url))
        if webcomic.donate_url:
            print("Donate: %s" % (webcomic.donate_url))

        # Check filesystem
        local_webcomic_archive_path = os.path.join(self.saved_archive_dir, parsed_args.webcomic)
        local_webcomic_volume_path = os.path.join(local_webcomic_archive_path, self.volume_info_dir)
        local_webcomic_compiled_path = os.path.join(self.compiled_comics_dir, parsed_args.webcomic)

        if not os.path.exists(local_webcomic_archive_path):
            print("Error: Archive not found, use the download command to download this comic's archive first.")
            return

        if not os.path.exists(self.compiled_comics_dir):
            os.mkdir(self.compiled_comics_dir)

        if os.path.exists(local_webcomic_compiled_path):
            shutil.rmtree(local_webcomic_compiled_path)
        os.mkdir(local_webcomic_compiled_path)

        if not os.path.exists(local_webcomic_volume_path):
            collections = True

        # Import support for chosen archive format
        if archive_type == "zip":
            from zip_archive import ZipArchive as ArchiveType

        # Get all comic pages
        comic_pages = glob.glob(os.path.join(local_webcomic_archive_path, "*.*"))
        comic_pages.sort()

        if not len(comic_pages):
            print("Error: No comic pages were found for compiling.")
            return

        volume_names = []

        # If we're doing volumes then get list of volume names
        if not collections:
            for file_path in glob.glob(os.path.join(local_webcomic_volume_path, "*")):
                volume_names.append(os.path.split(file_path)[-1])
            # Fallback
            if not len(volume_names):
                print("No volume info found for webcomic %s - generating collections instead." % webcomic.name)
                collections = True

        # If generating collections, get a list of desired names
        if collections:
            volume_names = []
            num_volumes = math.ceil(len(comic_pages) / pages_per_collection)
            justify = len(str(num_volumes))
            for vol_num in range(num_volumes):
                volume_names.append(str(vol_num + 1).rjust(justify, "0"))

        # Generate each archive
        volume_names.sort()
        for index, volume in enumerate(volume_names):
            print(" ")
            volume = WebcomicCompilerApp.escape_filename(volume)
            archive = ArchiveType()

            # Get a list of filenames to put in the archive
            comics_in_archive = []
            if collections:
                start = index*pages_per_collection
                comics_in_archive = comic_pages[start:start+pages_per_collection]
            else:
                volume_info_filename = os.path.join(local_webcomic_volume_path, volume)
                if not os.path.exists(volume_info_filename):
                    print("Error: Volume information file %s not found" % (volume_info_filename))
                    continue
                with open(volume_info_filename, 'r') as f:
                    comics_in_archive = f.read().strip().split("\n")
                comics_in_archive = [os.path.join(local_webcomic_archive_path, name) for name in comics_in_archive]

            if not len(comics_in_archive):
                print("Error: No comics to add to archive %s" % (volume))
                continue

            # Add each file to the archive
            justify = len(str(len(comics_in_archive)))
            for index, local_comic_filename in enumerate(comics_in_archive):
                file_extension = local_comic_filename.split(".")[-1]
                archive_comic_filename = str(index+1).rjust(justify, "0") + "." + file_extension
                archive.add_file(local_comic_filename, archive_comic_filename)

            # Spit out the final archive
            archive_name = "%s %s" % (webcomic.name, volume)
            print("Adding %s pages to archive %s" % (len(comics_in_archive), archive_name)),
            archive.output(os.path.join(local_webcomic_compiled_path, archive_name))

    def add_alt_text_to_page(self, image, alt_text):
        """
        Adds the passed alt text to a PIL image and returns it.
        """
        # Set up PIL
        font = ImageFont.truetype(self.alt_text_font, self.alt_text_size)
        draw = ImageDraw.Draw(image)

        # Get wrapped text
        character_width = draw.textsize("W", font=font)[0]
        width_of_lines = int((image.width-self.alt_text_padding*2) / character_width)
        alt_text = "\n".join(textwrap.wrap(alt_text, width_of_lines))

        # Resize image to add enough height for alt text
        text_size = draw.multiline_textsize(alt_text, font=font)
        new_height = image.height + text_size[1] + (self.alt_text_padding*2)
        resized_image = Image.new("RGBA", (image.width, new_height), (255,255,255,255))
        resized_image.paste(image, (0,0,image.width, image.height))
        draw = ImageDraw.Draw(resized_image)

        # Pop the text on
        draw.text((self.alt_text_padding, image.height+self.alt_text_padding), alt_text, fill=(0,0,0), font=font)

        return resized_image

    @classmethod
    def get_webcomic_object(cls, name):
        """
        Pass a webcomic's name to create the webcomic
        definition object.
        """
        webcomic_module = __import__(
            "webcomics.%s" % (name),
            globals(),
            locals(),
            ['Webcomic'],
            0
            )
        return webcomic_module.Webcomic()

    @classmethod
    def read_url_content(cls, url, retries, stream=False):
        """
        Returns a MechanicalSoup request containing the contexnt in a URL
        """
        retries_left = retries
        while retries_left >= 0:
            print("Fetching URL %s" % (url))
            try:
                res = WebcomicCompilerApp.browser.get(url, stream=stream)
                return res
            except (ConnectionError, HTTPError, Timeout) as e:
                print("Error fetching URL (%s)" % (e))
                retries_left -= 1
        return None

    @classmethod
    def escape_filename(cls, filename):
        """Escapes a filename via a character whitelist."""
        whitelist = "-_.() %s%s" % (string.ascii_letters, string.digits)
        return ''.join(char for char in filename if char in whitelist)

if __name__ == "__main__":
    WebcomicCompilerApp()
