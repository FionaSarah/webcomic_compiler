#!/usr/bin/env python
"""
-------------------------------
Webcomic Compiler
2016 Summers Development
-------------------------------

Base class for webcomic definitions.
Subclass any webcomics with this and fill in the gaps.
"""

class WebcomicBase(object):
    name = "Webcomic Name"
    author = "Author"
    url = "http://asupercool.comic"
    donate_url = None

    def archive_start_url(self):
        """
        Return a url to the first comic
        """
        raise NotImplementedError("archive_start_url method not implemented.")

    def get_image_url_from_page(self, content):
        """
        HTML content for a comic page is passed, it should
        return a url to the image file for the comic on this page.
        Return None if there is not a valid image on the page.
        """
        raise NotImplementedError("get_image_url_from_page method not implemented.")

    def get_next_url_from_page(self, content):
        """
        HTML content for a comic page is passed, it should
        return a url to the next page..
        Return None if this is the last page.
        """
        raise NotImplementedError("get_next_url_from_page method not implemented.")

    def get_alt_text_from_page(self, page_content):
        """
        Optional method.
        Returns a string containing alt text to append on the bottom
        of a page.
        Returns None if there's no alt text.
        """
        pass

    def before_download(self):
        """
        Optional method.
        Called before downloading.
        """
        pass

    def after_download(self):
        """
        Optional method.
        Called after downloading.
        """
        pass

    def get_volume_for_page(self, page_url, page_content):
        """
        Optional method.
        Gets the volume or chapter for a page given, this will
        influence the filename that the comic will be compiled
        under.
        Return None if not part of any volume.
        """
        pass
