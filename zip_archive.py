#!/usr/bin/env python
"""
-------------------------------
Webcomic Compiler
2016 Summers Development
-------------------------------

Support for CBZ files.
"""

from zipfile import ZipFile, ZIP_DEFLATED

class ZipArchive(object):

    pages = []

    def __init__(self):
        self.pages = []

    def add_file(self, local_file_path, archive_file_path):
        self.pages.append((local_file_path, archive_file_path))

    def output(self, archive_path):
        print("Creating file %s" % (archive_path + ".cbz"))
        with ZipFile(archive_path + ".cbz", "w", ZIP_DEFLATED) as archive:
            for local_path, archive_path in self.pages:
                archive.write(local_path, archive_path)
