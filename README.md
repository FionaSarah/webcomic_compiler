Webcomic Compiler - Archive Thief
=================================

This is a tool for downloading archives of webcomics and compiling them into a format usable by comic book readers.

It only supports a small number of webcomics, make a request in the issue tracker if you would like me to add support for a webcomic. Pull requests are accepted.

You probably shouldn't use this tool, it involves using a significant amount of bandwidth from webcomics and  blocks ad impressions. If you must use it then please consider donating to whichever webcomics you download.


Supported webcomics
===================

 * Dumbing Of Age (http://www.dumbingofage.com)
 * Scary Go Round (http://www.scarygoround.com/sgr)
 * Bad Machinery (http://www.scarygoround.com)
 * General Protection Fault (http://www.gpf-comics.com)
 * Questionable Content (http://questionablecontent.net)
 * Paranatural (http://paranatural.net)
 * Atomic Robo (http://www.atomic-robo.com)

Installation
============

Requirements: **Python 3.x, PIP**

Only tested on Arch Linux.

    git clone https://FionaSarah@bitbucket.org/FionaSarah/webcomic_compiler.git
    cd webcomic_compiler
    pip install -r requirements.txt


Usage
=====

This tool has two main tasks - downloading webcomic archives and compiling those them into comic book reader files.

For the purposes of illustration Scary Go Round will be used in all the following examples, substitute 'scarygoround' for the name of which comic you want to get.


List Comics
===========

Lists supported webcomics.

    python . list


Downloading Webcomics
=====================

Arguments can be combined as desired.

Download Entire Archive
-----------------------

Starts at the first page of a comic and downloads it's entire archive.

    python . download scarygoround

Resume Archive Download
-----------------------

If downloading an archive is interrupted and stops part way, this flag can be used to start from the last page downloaded. Using this will ignore the --start argument.

    python . download scarygoround --resume

Change Start Point
------------------

Normally archive downloads will start at the first page, pass in the URL of a particular page to start from that one. (This is useful if you are behind and want to catch up to newest pages.)

    python . download scarygoround --start "http://www.scarygoround.com/sgr/ar.php?date=20021114"

Limit How Many Pages Download
-----------------------------

Puts a limit on how many pages will be downloaded from an archive.

    python . download scarygoround --limit 20

Change Sleep Time
-----------------

By default there will be a 2 second wait between requesting new pages in an attempt to not tax the server too much, you can override this.

    python . download scarygoround --sleep 5

Change Download Retries
-----------------------

By default failed downloads will be retried 5 times before giving up. If you're dealing with a pretty bad server you can override this.

    python . download scarygoround --retry 10

Image Format
------------

The default image format pages are saved as is PNG. You can change this if necessary by supplying the file extension.

    python . download scarygoround --image_type tiff

Removing Alt-Text
-----------------

Sometimes comics have little additional bits of text, normally this will be added to the bottom of pages, you can use this flag to stop it being added.

    python . download paranatural --no_alt_text


Compiling Webcomics
===================

Once you have downloaded however much of a comic's archive desired you can compile them into archive files.

They can either be compiled into volumes or collections. Volumes are chapters or specific time periods as if they are comic issues. Collections are just chunks of pages.

The resulting files are dropped into a directory called "compiled".

Compile Volumes
---------------

Turns all downloaded pages into volumes that make sense for the comic. Each resulting file will be a chapter or period of time if a comic has no chapters.

    python . compile scarygoround

Compile Collections
-------------------

This turns all pages into archives containing a specific number of pages, by default this is 100.

    python . compile scarygoround --collections

Change Collection Page Count
----------------------------

Changes how many pages will appear in each collection.

    python . compile scarygoround --collections --pages_per_collection 30
