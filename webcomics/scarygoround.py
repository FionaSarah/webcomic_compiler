#!/usr/bin/env python
"""
-------------------------------
Webcomic Compiler
2016 Summers Development
-------------------------------

Definition for Scary Go Round.
(http://www.scarygoround.com)
"""

import urllib
from collections import OrderedDict
from datetime import datetime
from bs4 import BeautifulSoup
from webcomic_base import WebcomicBase

class Webcomic(WebcomicBase):
    name = "Scary Go Round"
    author = "John Allison"
    url = "http://www.scarygoround.com"
    donate_url = "http://www.scarygoround.com/subscriptions.php"

    sgr_archive_base_url = "http://www.scarygoround.com/sgr/ar.php"
    sgr_archive_start = "?date=20020604"
    sgr_archive_image_base_url = "http://www.scarygoround.com/sgr/"
    sgr_archive_chapter_page = "http://www.scarygoround.com/sgr/index.php"
    sgr_chapters = None

    def before_download(self):
        # We get a list of chapters and their respective dates.
        # Later we can campare the date of each page to work out which chapter
        # they go in.
        request = urllib.request.Request(self.sgr_archive_chapter_page)
        response = urllib.request.urlopen(request)
        archive_content = response.read()
        soup = BeautifulSoup(archive_content, "html.parser")
        self.sgr_chapters = OrderedDict()
        chapter_num = 1
        for a in soup.select("table td a"):
            chapter_date = a['href'].split("=")[-1]
            chapter_num_str = str(chapter_num).rjust(2, '0')
            self.sgr_chapters[chapter_num_str] = datetime.strptime(chapter_date, "%Y%m%d")
            chapter_num += 1

    def archive_start_url(self):
        return self.sgr_archive_base_url + self.sgr_archive_start

    def get_image_url_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        img_tag = soup.select('img[src^="strips/"]')
        if not len(img_tag):
            return None
        return self.sgr_archive_image_base_url + img_tag[0]['src']

    def get_next_url_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        for a in soup.find_all("a", text="Next"):
            return self.sgr_archive_base_url + a['href']
        return None

    def get_volume_for_page(self, page_url, page_content):
        cur_page_date = datetime.strptime(page_url.split("=")[-1], "%Y%m%d")
        potential_chapter = list(self.sgr_chapters.items())[0][0]
        for chapter_num, chapter_date in self.sgr_chapters.items():
            if chapter_date > cur_page_date:
                return potential_chapter
            potential_chapter = chapter_num
        return potential_chapter
