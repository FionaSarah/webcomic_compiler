#!/usr/bin/env python
"""
-------------------------------
Webcomic Compiler
2016 Summers Development
-------------------------------

Definition for Paranatural.
(http://paranatural.net)
"""

from webcomic_base import WebcomicBase
from bs4 import BeautifulSoup
from __main__ import WebcomicCompilerApp
import re

class Webcomic(WebcomicBase):
    name = "Paranatural"
    author = "Zack Morrison"
    url = "http://www.paranatural.net"
    donate_url = "http://www.patreon.com/paranatural"

    stories_page = "http://www.paranatural.net/archive-2"
    chapters = {}

    def before_download(self):
        content = WebcomicCompilerApp.read_url_content(self.stories_page, 5)
        if content is None:
            print("Can't get volume info")
            return
        soup = BeautifulSoup(content.text, "html.parser")
        for title_tag in soup.select('p a[title^="Chapter "]'):
            title = re.match(
                "Ch.(\d) - (.*)",
                title_tag.parent.next_sibling.next_sibling.get_text()
                )
            self.chapters[int(title.group(1))] = title.group(2)

    def archive_start_url(self):
        return "http://www.paranatural.net/comic/chapter-1"

    def get_image_url_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        img_tag = soup.select("div#cc-comicbody img")
        if not len(img_tag):
            return None
        return img_tag[0]['src']

    def get_next_url_from_page(self, page_content):
        soup = BeautifulSoup(page_content, "html.parser")
        a_tag = soup.select("div.nav a.next")
        if not len(a_tag):
            return None
        return a_tag[0]['href']

    def get_alt_text_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        img_tag = soup.select("div#cc-comicbody img")
        if not len(img_tag):
            return None
        alt_text =  img_tag[0]['title']
        title = re.match(
            "Paranatural - Guest Comic(.*)",
            soup.title.string
            )
        if title is not None:
            alt_text = "Guest comic " + title.group(1).strip() + " - " + alt_text
        return alt_text

    def get_volume_for_page(self, page_url, page_content):
        soup = BeautifulSoup(page_content, "html.parser")
        title = re.match(
            "Paranatural - Chapter (\d)(.*)",
            soup.title.string
            )
        if title is None:
            # Fall back to guest comics
            title = re.match(
                "Paranatural - Guest Comic.*",
                soup.title.string
                )
            if title is not None:
                return "%s - Guest Comics" % (len(self.chapters) + 1)
        else:
            volume = int(title.group(1))
            return "%s - %s" % (volume, self.chapters[volume])
        return None
