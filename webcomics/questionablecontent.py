#!/usr/bin/env python
"""
-------------------------------
Webcomic Compiler
2016 Summers Development
-------------------------------

Definition for Questionable Content.
(http://questionablecontent.net)
"""

from webcomic_base import WebcomicBase
from bs4 import BeautifulSoup

class Webcomic(WebcomicBase):
    name = "Questionable Content"
    author = "Jeph Jacques"
    url = "http://questionablecontent.net"
    donate_url = "http://www.patreon.com/jephjacques"

    qc_base_url = "http://questionablecontent.net/"

    def archive_start_url(self):
        return self.qc_base_url + "view.php?comic=1"

    def get_image_url_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        img_tag = soup.select("img#strip")
        if not len(img_tag):
            return None
        return self.qc_base_url + img_tag[0]['src']

    def get_next_url_from_page(self, page_content):
        soup = BeautifulSoup(page_content, "html.parser")
        for a in soup.select("ul#comicnav li a"):
            if a.string == "Next":
                if a['href'] == "#":
                    return None
                return self.qc_base_url + a['href']
        return None


# questionable artstyle more like it
