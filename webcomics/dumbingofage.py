#!/usr/bin/env python
"""
-------------------------------
Webcomic Compiler
2016 Summers Development
-------------------------------

Definition for Dumbing Of Age
(http://www.dumbingofage.com)
"""

import urllib
from bs4 import BeautifulSoup
from webcomic_base import WebcomicBase

class Webcomic(WebcomicBase):
    name = "Dumbing Of Age"
    author = "David M Willis"
    url = "http://www.dumbingofage.com"
    donate_url = "https://www.patreon.com/dumbingofage"

    doa_archive_start = "http://www.dumbingofage.com/2010/comic/book-1/01-move-in-day/home/"
    doa_archive_chapter_page = "http://www.dumbingofage.com/archive-2/"
    doa_chapters = None

    def before_download(self):
        request = urllib.request.Request(self.doa_archive_chapter_page)
        response = urllib.request.urlopen(request)
        archive_content = response.read()
        soup = BeautifulSoup(archive_content, "html.parser")
        self.doa_chapters = []
        books = soup.select('ul#storyline ul.level-1 li[id^="storyline-book-"]')
        for book in books:
            book_title = book.select("a.storyline-title")[0].string
            book_li = book.find_next("li")
            for chapter_tag in book_li.select("a.storyline-title"):
                self.doa_chapters.append(
                    (chapter_tag.string, book_title)
                    )

    def archive_start_url(self):
        return self.doa_archive_start

    def get_image_url_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        img_tag = soup.select('div.comicpane img')
        if not len(img_tag):
            return None
        return img_tag[0]['src']

    def get_next_url_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        a_tag = soup.select('div.comic_navi_wrapper a.navi-next')
        if not len(a_tag):
            return None
        return a_tag[0]['href']

    def get_volume_for_page(self, page_url, page_content):
        soup = BeautifulSoup(page_content, "html.parser")
        current_chapter_name = soup.select("div.post-info li.storyline-root a")[0].string
        justify = len(str(len(self.doa_chapters)))
        for index, chap in enumerate(self.doa_chapters):
            chapter_name, book_name = chap
            if chapter_name == current_chapter_name:
                return "%s - %s - %s" % (str(index+1).rjust(justify, "0"), book_name, chapter_name)
