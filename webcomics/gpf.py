#!/usr/bin/env python
"""
-------------------------------
Webcomic Compiler
2016 Summers Development
-------------------------------

Definition for Bad Machinery.
(http://www.scarygoround.com)
"""

import re
import urllib
from datetime import datetime
from bs4 import BeautifulSoup
from webcomic_base import WebcomicBase
from collections import namedtuple

GPFStory = namedtuple("GPFStory", ['num', 'year', 'date', 'name'])

class Webcomic(WebcomicBase):
    name = "General Protection Fault"
    author = "Jeff Darlington"
    url = "http://www.gpf-comics.com"
    donate_url = "http://www.gpf-comics.com/tips.php"

    gpf_base_url = "http://www.gpf-comics.com"
    gpf_first_page = "/archive/1998/11/02"
    gpf_story_index = "/archive/stories.php"
    gpf_stories = None

    def before_download(self):
        # We get a list of stories and their respective dates.
        # Later we can campare the date of each page to work out which chapter
        # they go in.
        request = urllib.request.Request(self.gpf_base_url + self.gpf_story_index)
        response = urllib.request.urlopen(request)
        content = response.read()
        soup = BeautifulSoup(content, "html.parser")
        self.gpf_stories = []
        story_num = 1
        for year_num, year_tag in enumerate(soup.select('div.bulktext a[name^="year"]')):
            year_stories_a = year_tag.find_next("dl").select("dt a")
            for story_a in year_stories_a:
                explode_date = story_a['href'].split("/")
                story_date = datetime(day=int(explode_date[-1]), month=int(explode_date[-2]), year=int(explode_date[-3]))
                s = GPFStory(num=story_num, year=(year_num+1), date=story_date, name=story_a.string)
                self.gpf_stories.append(s)
                story_num += 1

    def archive_start_url(self):
        return self.gpf_base_url + self.gpf_first_page

    def get_image_url_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        img_tag = soup.select('img[src^="/comics/"]')
        if not len(img_tag):
            return None
        return self.gpf_base_url + img_tag[0]['src']

    def get_next_url_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        a_tag = soup.select('span.nav_link_forward a')
        if not len(a_tag):
            return None
        return a_tag[0]['href']

    def get_volume_for_page(self, page_url, page_content):
        explode_date = page_url.split("/")
        try:
            page_date = datetime(day=int(explode_date[-1]), month=int(explode_date[-2]), year=int(explode_date[-3]))
        except ValueError:
            # Couldn't get date from URL - try to get from page
            soup = BeautifulSoup(page_content, "html.parser")
            unparsed_date = soup.select("div.strip_header")[0].text.strip()
            regs = re.match(r"^.*?,\s*(.*?)\s+(\d+)\s*,\s*(\d+)\s*$", unparsed_date)
            page_date = datetime.strptime(
                "%s %02d %s" % (regs.group(1), int(regs.group(2)), regs.group(3)),
                "%B %d %Y")
        potential_story = self.gpf_stories[0]
        for story in self.gpf_stories:
            if story.date > page_date:
                return self.story_name_format(potential_story)
            potential_story = story
        return self.story_name_format(potential_story)

    def story_name_format(self, story):
        justify = len(str(len(self.gpf_stories)))
        index = str(story.num).rjust(justify, '0')
        return "%s - Year %s - %s" % (index, story.year, story.name)
