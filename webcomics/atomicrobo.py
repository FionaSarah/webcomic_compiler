#!/usr/bin/env python
"""
-------------------------------
Webcomic Compiler
2016 Summers Development
-------------------------------

Definition for Atomic Robo
(http://www.scarygoround.com)
"""

import re
import urllib
from bs4 import BeautifulSoup
from webcomic_base import WebcomicBase
from collections import namedtuple
from __main__ import WebcomicCompilerApp

class AtomicRoboIssue:
    num = 0
    prefix = ""
    name = ""
    book = ""
    def __init__(self, num = 0, prefix = "", name = "", book = ""):
        self.num, self.prefix, self.name, self.book = num, prefix, name, book

    def __str__(self):
        return self.__repr__()
    def __repr__(self):
        return "[ {}, {}, {}, {} ]".format(self.num, self.name, self.prefix, self.book)

class Webcomic(WebcomicBase):
    name = "Atomic Robo"
    author = "Brian Clevinger & Scott Wegener"
    url = "http://www.atomic-robo.com"
    donate_url = "http://patreon.com/tesladyne"

    base_url = "http://www.atomic-robo.com/atomicrobo/"
    start_page = "v1ch1-cover"
    archive_page = "archive"
    issues = []

    def before_download(self):
        content = WebcomicCompilerApp.read_url_content(self.base_url + self.archive_page, 5)
        if content is None:
            print("Can't get volume info")
            return
        self.issues = []
        soup = BeautifulSoup(content.text, "html.parser")
        selector = 'div.atomicrobo-chapterbox div.cc-chapterrow a[href^=%s]' % self.base_url
        for issue_link in soup.select(selector):
            # Go up and backwards till we get the book name
            book = ""
            cur_tag = issue_link.parent.parent.parent
            while True:
                cur_tag = cur_tag.previous_sibling
                if cur_tag['class'][0] == "cc-chapterrow":
                    book = cur_tag.text
                    break
            # Grab prefix
            prefix = self.get_prefix_from_url(issue_link['href'])
            # Create issue
            issue = AtomicRoboIssue(
                prefix=prefix, name=issue_link.string, book=book
                )
            self.issues.append(issue)

        # They're in the wrong order on the page
        self.issues.reverse()

        # Update nums
        num = 1
        for issue in self.issues:
            issue.num = num
            num += 1

    def archive_start_url(self):
        return self.base_url + self.start_page

    def get_image_url_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        img_tag = soup.select("img#cc-comic")
        if not len(img_tag):
            return None
        return img_tag[0]['src']

    def get_next_url_from_page(self, content):
        soup = BeautifulSoup(content, "html.parser")
        a_tag = soup.select("div.nav a.next")
        if not len(a_tag):
            return None
        return a_tag[0]['href']

    def get_volume_for_page(self, page_url, page_content):
        prefix = self.get_prefix_from_url(page_url)
        chosen_issue = None
        for issue in self.issues:
            if issue.prefix == prefix:
                chosen_issue = issue
                break
        if not chosen_issue is None:
            justify = len(str(len(self.issues)))
            index = str(chosen_issue.num).rjust(justify, '0')
            return "{} - {} - {}".format(index, chosen_issue.book, chosen_issue.name)
        return None

    def get_prefix_from_url(self, url):
        regs = re.match(r"^.*\/([a-zA-Z0-9]+)(-cover|-page-\d+|-page-\w+|-\d+|-\w+)$", url)
        if regs is None:
            second_regs = re.match(r"^.*\/([a-zA-Z0-9-]+)(-cover|-\d+)$", url)
            return second_regs.group(1)
        return regs.group(1)


# Jesus christ this website had no system at all, I'm shocked I managed to compile
# it into volumes at all.
